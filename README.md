# How to use
  
## Launch it  
> Deploy centreon :  
`ansible-playbook --diff -i hosts.ini plays/centreon-provisioning.yml -l centreon01`  
> Deploy LAMP :  
`ansible-playbook --diff -i hosts.ini plays/lamp-provisioning.yml -l web01`  
  
> for all the centreons :  
`ansible-playbook --diff -i hosts.ini plays/centreon-provisioning.yml -l centreon`  
> for all the lamps :  
`ansible-playbook --diff -i hosts.ini plays/lamp-provisioning.yml -l web`  
  
## Dry run + debug  
`ansible-playbook --diff --check -v -i hosts.ini plays/centreon-provisioning.yml -l centreon01`  
`ansible-playbook --diff --check -v -i hosts.ini plays/lamp-provisioning.yml -l web01`  
