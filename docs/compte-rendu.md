# Documentation projet supervision


| Nom                       | IPv4            | Role                                      | Identifiants  | Hôte |
|---------------------------|-----------------|-------------------------------------------|---------------|------|
| centreon.lab.local        | 192.168.181.129 | Supervision hybridge Linux/Windwos        | admin/ingesup | kvm1 |
| iMC_NAT                   | 192.168.181.128 | Système de surveillance et gestion réseau | admin/admin   | kvm1 |
| web.lab.local             | 192.168.181.130 | Serveur LAMP                              | -             | kvm1 |
| ansible.lab.local         | 192.168.181.131 | Machine d'administration Ansible          | -             | kvm1 |
| CORE_SW_01.ingesup.local  | 192.168.181.101 | -                                         | admin/ingesup | gns3 |
| DISTRI_SW01.ingesup.local | 192.168.181.102 | -                                         | admin/ingesup | gns3 |
| DISTRI_SW02.ingesup.local | 192.168.181.103 | -                                         | admin/ingesup | gns3 |
| ACCESS_SW01.ingesup.local | 192.168.181.104 | -                                         | admin/ingesup | gns3 |


## Utilisateurs

Windows server session : Administrateur/Passw0rd  
Routers root session : admin/ingesup  
Centreon Interface User : admin/ingesup  
Centreon Mysql : centreon/ingesup & root/[N/A]  
  
## GNS3_SERVER + KVM  

kvm1.dotfile.eu : hosting gns3 server + qemu://kvm  
we use : ssh -L 8080:127.0.0.1:3080 gabyf@kvm1.dotfile.eu  
to get kvm1.dotfile.eu:3080 flow on our 8080 local port and use GNS3 project on remote.  

## Configuration SNMP

```
no ip http server
no ip http secure-server
!
snmp-server community INGESUP RW
snmp-server community INGESUP_RO RO
snmp-server enable traps snmp authentication linkdown linkup coldstart warmstart
snmp-server enable traps vrrp
snmp-server enable traps ds1
snmp-server enable traps tty
snmp-server enable traps eigrp
snmp-server enable traps atm subif
snmp-server enable traps config-copy
snmp-server enable traps config
snmp-server enable traps event-manager
snmp-server enable traps frame-relay multilink bundle-mismatch
snmp-server enable traps frame-relay
snmp-server enable traps frame-relay subif
snmp-server enable traps hsrp
snmp-server enable traps ipmulticast
snmp-server enable traps mpls ldp
snmp-server enable traps mpls traffic-eng
snmp-server enable traps msdp
snmp-server enable traps ospf state-change
snmp-server enable traps ospf errors
snmp-server enable traps ospf retransmit
snmp-server enable traps ospf lsa
snmp-server enable traps ospf cisco-specific state-change nssa-trans-change
snmp-server enable traps ospf cisco-specific state-change shamlink interface
snmp-server enable traps ospf cisco-specific state-change shamlink neighbor
snmp-server enable traps ospf cisco-specific errors
snmp-server enable traps ospf cisco-specific retransmit
snmp-server enable traps ospf cisco-specific lsa
snmp-server enable traps pim neighbor-change rp-mapping-change invalid-pim-message
snmp-server host 192.168.181.128 INGESUP 
mac-address-table static c201.2aa8.0000 interface FastEthernet1/1 vlan 100
no cdp log mismatch duplex
```

## iMC_NAT

we installed windows 10 vm + iMC from sources   
we deactivated windows firewall + windows defender on :   
Gestionnaire de serveur > serveur local > pare-feu windows + windows defender + internet explorer protection. 

## Centreon + Centos web  

To reach Centreon web UI :   
`ssh -D 9009 gabyf@kvm1.dotfile.eu`  
Firefox :  
`about:preferences`  
`use network settings and use SOCKS proxy`  
we installed centos7 with centreon with Ansible-playbook
we installed centos7 host to monitor

## Ansible HOST

git clone https://gitlab.dotfile.eu/gaby/Supervision.git
run all playbooks.

# Compte-rendu du TP

## IMC

```
- Prendre en main l'outil de Monitoring & Management HPE IMC
	- Découvrir les différents menus
	- Résumez les en expliquant leur fonctionnalités majeures
```


* Resource

Configuration des devices et de la topologie réseau
Configuration des "Custom View" permet un affichage personalisable pour les différents segments d'une topologies

* User

Gestion des utilisateurs

* Service

Différents services d'IMC:
Templates de configuration
Backups
VLAN/VXLAN/ACL/Airwave WLAN

* Report

Génération de rapport
Configuration des templates de report

* Alarm

Le résumé des alarmes / Trap SNMP

* System

Configuration du serveur IMC
Group de devices
RBAC

```
- Configuration d'une vue personnaliée
        - Configurer une vue personnalisée incluant l'ensemble des routeurs, appellée "TP_SUPERVISION"
```

* Vue personnalisée

![custom-view-creation](images/custom-view-parameter.png "Creation de la vue personnalisée")
![from-customview-get-topology](images/custom-view-list-result.png "Voir la topology de l'infra depuis la vue personnalisée")


```
- Configuration des Templates d'accès
        - Configurer les credentials généraux pour le SNMP
        - Configurer les credentials généraux pour le SSH
```

![accéder-aux-templates](images/access-paramter-template.png "Accéder aux configurations des templates")
![differents-templates](images/differents-templates.png "Les templates que l'on va modifier")

* Templates SNMP

![Add-snmp-template](images/snmp-template-added.png "Ajout du template SNMP")

* Templates SSH

![Add-ssh-template](images/ssh-template-added.png "Ajout du template SSH")

* Redeploy Templates on devices

![deploy-templates-on-devices](images/deploy-templates-on-devices "Déployer les templates de confs aux devices")


```
- Configuration par CLI Script
        - Configuration libre d'un élément d'un ou plusieurs switchs grâce à une batch operation / CLI Script
```

![](images/cli-script01.png)
![](images/cli-script02.png)

```
- Management des Alarmes
        - Décocher les alarmes sur les ports access
        - Configurer des alarmes personnalisées
```

```
- Management des VLANS
        - Deployez un VLAN sur l'ensemble des switchs via le VLAN Deployment task
                - Verifier la bonne création de ceux-ci grace à la commande "show vlan-switch" sur chaque routeur
        - Taggez le vlan déployé sur les interconnexion via la Network Topology grace à la "vlan view"
```

![](./images/vlan01.png)
![](./images/vlan02.png)
![](./images/vlan03.png)
![](./images/vlan04.png)
![](./images/vlan05.png)

```
CORE_SW_01#show vlan-switch 

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Fa1/3, Fa1/4, Fa1/5, Fa1/6, Fa1/7, Fa1/8, Fa1/9, Fa1/10, Fa1/11, Fa1/12
                                                Fa1/13, Fa1/14, Fa1/15
10   USERS                            active    
20   SERVEURS                         active    
30   CAMERAS                          active    
40   TELEPHONIE                       active    
50   DMZ                              active    
60   INDUSTRIEL                       active    
70   VIP                              active    
80   GUEST                            active    
90   IMPRIMANTES                      active    
100  MANAGEMENT                       active    Fa1/0
200  VLAN_200                         active    
1002 fddi-default                     act/unsup 
1003 token-ring-default               act/unsup 
1004 fddinet-default                  act/unsup 
1005 trnet-default                    act/unsup 

VLAN Type  SAID       MTU   Parent RingNo BridgeNo Stp  BrdgMode Trans1 Trans2
---- ----- ---------- ----- ------ ------ -------- ---- -------- ------ ------
1    enet  100001     1500  -      -      -        -    -        1002   1003
10   enet  100010     1500  -      -      -        -    -        0      0   
20   enet  100020     1500  -      -      -        -    -        0      0   
30   enet  100030     1500  -      -      -        -    -        0      0   
40   enet  100040     1500  -      -      -        -    -        0      0   
50   enet  100050     1500  -      -      -        -    -        0      0   
60   enet  100060     1500  -      -      -        -    -        0      0   
70   enet  100070     1500  -      -      -        -    -        0      0   
80   enet  100080     1500  -      -      -        -    -        0      0   
90   enet  100090     1500  -      -      -        -    -        0      0   
100  enet  100100     1500  -      -      -        -    -        0      0   
200  enet  100200     1500  -      -      -        -    -        0      0   
1002 fddi  101002     1500  -      -      -        -    -        1      1003
1003 tr    101003     1500  1005   0      -        -    srb      1      1002
1004 fdnet 101004     1500  -      -      1        ibm  -        0      0   
1005 trnet 101005     1500  -      -      1        ibm  -        0      0

```

```
- Management des Configurations
	- Premiere sauvegarde de la running + startup configuration
	- Sauvegarde Automatique tous les jours à une heure de votre choix
```

## Centreon

```
- Mise en place d'une VM Centreon:
        - Télécharger et Installer Centreon en VM
```

* Pré requis

Avant de commencer, nous savions que nous allions utiliser Ansible afin de déployer les serveurs rapidement  
et d'automatiser notre infrastructure donc nous avons créé un serveur, toujours dans le réseau 192.168.181.0 
afin qu'il soit notre Ansible master.
Nous avons crée un utilisateur Ansible, et déployer sa clé ssh sur les différents serveurs de notre infra.
(Centreon & LAMP).
Ansible à l'aide de playbooks permet en fait d'exécuter une multitude d'action au travers de SSH sur un host
ou plusieurs hosts.
Cela nous a permis de cloisonner les services. 


* Déploiement de Centreon

Etant donné que notre Hyperviseur (KVM) est hébergé chez Online, et que notre GNS3 server l'est aussi, 
nous avons juste eu besoin de créer une vm centos 7 fresh installed dans le bon réseau KVM link au 
Cloud1, puis nous avons développé un playbook Ansible avant de le déployer.
Dans notre cas le playbook "centreon-provisioning.yml" met à jour Centos, intalle certains paquets 
nécessaires puis nous installe les dépendances de Centreon. (Httpd, mariaDB, PHP, nagios....)

Pour déployer Centreon : (Depuis le ansible-master)  
`ansible-playbook --diff -i hosts.ini plays/centreon-provisioning.yml -l centreon01`


```
- Mise en place d'une CentOS avec un LAMP
         - Installation de l'OS
         - Configuration du LAMP
         - Publication d'une page HTTP
```
  
* Déploiement d'une stack LAMP 
  
Pour la stack LAMP à déployer (Linux, apache, mysql, php), nous avons également utiliser un playbook Ansible
que nous avons développé. Il permet d'automatiser l'infra et en cas de problème de la remonter rapidement.
  
Pour déployer LAMP : (Depuis le ansible-master)  
`ansible-playbook --diff -i hosts.ini plays/lamp-provisioning.yml -l web01`

Le LAMP publie seulement un index.php presque vide qui affiche seulement un gif.
Le but étant juste de superviser une webapp au travers de requêtes HTTP. 
  
```
         - Connexion de la VM dans GNS3 de la même façon que la VM IMC (utiliser le même cloud et Vmnet)
         - Configurer les checks de bases sur les switchs (télécharger les plugins gratuits si necessaire)
```

* Configuration de Base 

Pour ce qui est de connecter la VM hébergeant la stack LAMP, elle est bien évidemment dans le même KVM
network que le reste du projet.  
![ping-web-lamp](images/ping-web-lamp.png "ping-web-lamp")

* Configurer les checks de Base pour les routeurs

Des plugins gratuits permettant de récupérer les bons OID directement sans devoir scripter nous-même sont
disponibles, on va les utiliser.
Le plugin net cisco snmp est pas mal. Il nous permet, en créant un "host_group" et des "services by group"
de faire en sorte qu'à chaque fois qu'on ajoute un routeur et qu'on le place dans le groupe Cisco3620, il 
se retrouve avec son CPU, sa RAM, son Traffic, et son Packet Loss supervisés.

Il faut pour se faire créer un host_group. Puis on créer des services avec les commandes et templates du plugin
net cisco snmp. Ces services sont en fait des services by "host_group" et vont s'appliquer à tout hosts dans ce
groupe.

On peut évidemment personnaliser les seuils pour les différentes alertes (warning, critical). Comme cela, on peut
setup des notifications (email, message, ...) si un service ateint le seul de warning/critical. 
Pour la ram des routeurs Cisco j'ai dû augmenter le seuil à 90% pour l'état "warning" car le routeur de base prend 83% de ram en règle générale. Si on n'avait pas changé ce seuil, on aurait été spammé de notifications et on
aurait eu de la supervision avec des false negative. 
A contrario, pour éviter les false positive, il faut ajuster les seuils pour chaque groupe de devices suivant
leur criticité et leur environnement (dev/preprod/prod).

Sans changer le seuil on se retrouve avec ça :  
![centreon-routeur-warning](images/centreon-routers-with-services-warning "centreon-routeur-warning")

Dans la finalité, voici notre monitoring des routeurs :  
![routeurs-centreon-well-set](images/centreon-routeurs-well-set "routeurs-centreon-well-set")

```
- Configuration de Notifications(en option)
        - Configurer envoi de mail
        - Configurer treshold et alarmes
```

* Configurer envoi de mail


* Configurer treshold et alarmes


```
- Configurer supervision d'un serveur Windows
        - Superviser via le WMI les métriques classiques
```

* Superviser via le WMI les métriques d'iMC

Pour faire cette partie-ci, on va installer le plugin Windows SNMP qui récupère lui aussi les bons
OID pour windows et qui propose donc les commandes par défaut pour le CPU, Disk, RAM.  

Faut aussi ne pas oublier d'ouvrir le port 162/161 sur le serveur windows.

On va aussi se baser un host_group qu'on va appeler windows, et si jamais on veut rajouter un windows
dans le parc de serveur, on pourra juste l'ajouter au groupe et comme les services seront associés
au groupe, on récupèrera directement sa RAM/CPU/DISK. 
![windows-wmi](images/windows-wmi.png)

Ensuite j'ai crée une custom command, qui exécute une requête http afin de vérifier que l'interface web
d'iMC est bien up, et qu'il n y a pas un problème au niveau du service en lui-même.

![imc-check](images/imc-check.png)

```
- Configurer la supervision d'un serveur Linux
        - Superviser les métriques classiques (Ram, CPU, Disk...)
        - Superviser un service HTTP à l'interieur de ce serveur
        - Superviser la valeur d'activation du SE LINUX (on/off) -- Optionnel
        - Superviser les services en execution sur l'OS -- Optionnel
```

* Superviser les métriques classiques

Pareil, installation du plugin Linux-snmp (fournissant les sondes nagios pour linux), puis on crée un 
host_group "linux" et on associe des services au host_group. 

![linux-snmp](images/linux-snmp)

* Superviser un service HTTP sur ce serveur

